# Enhancements that can be made to this script

* Create security group with ssh permissions via script rather than prerequisite
* Allow config for aws key that can override shared config file
* Collect common behavior into functions instead of duplicating between instanceA and instanceB
* Improve policy for s3 bucket to be more restrictive