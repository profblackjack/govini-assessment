# govini-assessment
See [Objectives](./Objectives.md) for details on content, and [Enhancements](./Enhancements.md) for optimizations and features that could improve usage.

## Prerequisites
* [Node v10+ with npm](https://nodejs.org/en/)
* [AWS credentials loaded into shared credentials file](https://docs.aws.amazon.com/sdk-for-javascript/v2/developer-guide/loading-node-credentials-shared.html)
    * Credentials should be for user with admin permissions for ease of use
* [A key pair created](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-key-pairs.html#having-ec2-create-your-key-pair) 
    * update "SSHKeyName" in config.json with name of key pair 
    * download private key to local machine
    * update "pathToSSHKey" in config.json
        * example: "/home/username/.ssh/govini-assessment-key.pem"
* A security group with ssh permissions, allowing at a minimum inbound connections from your machine's IP on TCP port 22 (allowing all IPs is ok for this demo, but unsafe for production)
    * [creating a security group](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-network-security.html#creating-security-group)
    * [adding ssh permissions to group](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/authorizing-access-to-an-instance.html)
    * add name of security group to the key "SSHEnabledSecurityGroup" in config.json

## Execution
```bash
# Install package dependencies
$ npm install

# Execute script with default credentials
$ node index.js

# alternatively, execute script with different profile credentials
$ AWS_PROFILE="alt-profile" node index.js

# To teardown after execution, use the output teardown package and the --teardown flag.
# Replace 'xxx' with the teardown package
$ node index.js --teardown 'xxx'
```

## EXAMPLE
```
$ node index.js
Begin creating instanceA...
Begin creating instanceB...

...

Packing params necessary for teardown ...
Script complete. To teardown, please execute 'node index.js --teardown xxx', where xxx is the following packed string (include single quotes):
'InstanceIds|i-0bc3fbdacb3200b97|i-0a81fcc7566c16700|Bucket|govini-assessment-bucket-59a6aa79-8bc3-4917-a3f7-9b034f515d4e|RoleName|S3Access-59a6aa79-8bc3-4917-a3f7-9b034f515d4e|VolumeIds|vol-0d738d3cdbb7c7b0e|vol-0d87d1fb9af4118e0|vol-05d2072e7617ce162^^^$0|@1|2]|3|4|5|6|7|@8|9|A]]'

done!

$ node index.js --teardown 'InstanceIds|i-0bc3fbdacb3200b97|i-0a81fcc7566c16700|Bucket|govini-assessment-bucket-59a6aa79-8bc3-4917-a3f7-9b034f515d4e|RoleName|S3Access-59a6aa79-8bc3-4917-a3f7-9b034f515d4e|VolumeIds|vol-0d738d3cdbb7c7b0e|vol-0d87d1fb9af4118e0|vol-05d2072e7617ce162^^^$0|@1|2]|3|4|5|6|7|@8|9|A]]'
Teardown flag supplied, attempting to read passed teardown argument ...

...

Volumes deleted. Teardown Complete!
done!
```