// external modules
const AWS = require('aws-sdk');
const node_ssh = require('node-ssh');
const uuidv4 = require('uuid/v4');

// internal modules
const instanceA = require('./src/InstanceA');
const instanceB = require('./src/InstanceB');
const createS3Bucket = require('./src/S3Bucket');
const { createRole, createInstanceProfile, addIPToInstance } = require('./src/InstanceProfile');
const s3DataTransferTest = require('./src/s3DataTransferTest');
const { buildTeardownParams, parseTeardownArg } = require('./src/teardownParamParse');
const teardown = require('./src/teardown');

// config.json
const { region, apiVersion, SSHKeyName, pathToSSHKey, SSHEnabledSecurityGroup, bucketName } = require('./config.json');

AWS.config.update({ region, apiVersion });

// services
const ec2 = new AWS.EC2();
const s3 = new AWS.S3();
const iam = new AWS.IAM();

const uniqueID = uuidv4(),
  Bucket = `${bucketName}-${uniqueID}`,
  VolumeAName = 'InstanceA_Volume',
  VolumeBName1 = 'InstanceB_Volume1',
  VolumeBName2 = 'InstanceB_Volume2';

// check for teardown flag
if (process.argv.length === 4 && process.argv[2] === '--teardown') {
  console.log('Teardown flag supplied, attempting to read passed teardown argument ...');
  return Promise.resolve(parseTeardownArg(process.argv[3]))
    .then(({ InstanceIds, Bucket, RoleName, VolumeIds }) => {
      console.log('Initiatig teardown ...');
      return teardown(ec2, s3, iam, InstanceIds, Bucket, RoleName, VolumeIds);
    }).catch((err) => console.error('Teardown failed with the following error: ', err))
    .finally(() => console.log('done!'));
} else {
  // run normal behavior
  Promise.all([
    instanceA(ec2, node_ssh, SSHKeyName, pathToSSHKey, SSHEnabledSecurityGroup, VolumeAName),
    instanceB(ec2, node_ssh, SSHKeyName, pathToSSHKey, SSHEnabledSecurityGroup, [VolumeBName1, VolumeBName2]),
    createRole(iam, uniqueID).then((RoleName) => createInstanceProfile(iam, RoleName)),
    createS3Bucket(s3, Bucket)
  ]).then(([InsA, InsB, RoleName]) => {
    return Promise.all([
      addIPToInstance(ec2, InsA.InstanceId, RoleName),
      addIPToInstance(ec2, InsB.InstanceId, RoleName)
    ]).then(() => s3DataTransferTest(node_ssh, Bucket, region, InsA.sshParams, InsB.sshParams))
      .then(() => buildTeardownParams([InsA.InstanceId, InsB.InstanceId], Bucket, RoleName, [...InsA.VolumeIds, ...InsB.VolumeIds]))
      .then((pack) => console.log(`Script complete. To teardown, please execute 'node index.js --teardown xxx', where xxx is the following packed string (include single quotes):\n'${pack}'\n`));
  }).catch((err) => console.error(err))
    .finally(() => console.log('done!'));
}