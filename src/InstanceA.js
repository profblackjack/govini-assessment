function instanceA_Params (SSHKeyName, SSHEnabledSecurityGroup) {
  return {
    ImageId: 'ami-0080e4c5bc078760e',
    InstanceType: 't2.micro',
    KeyName: SSHKeyName,
    MinCount: 1,
    MaxCount: 1,
    SecurityGroupIds: [SSHEnabledSecurityGroup],
    BlockDeviceMappings: [
      {
        // Root Volume
        DeviceName: "/dev/xvda",
        Ebs: {
          DeleteOnTermination: true,
          VolumeSize: 9 // GiB
        }
      }
    ],
    TagSpecifications: [
      {
        ResourceType: 'instance',
        Tags: [
          {
            Key: 'Name',
            Value: 'InstanceA'
          }
        ]
      }
    ]
  };
}
function instanceA_VolumeParams(VolumeAName) {
  return {
    Encrypted: false,
    Size: 1, //GiB
    VolumeType: 'gp2',
    TagSpecifications: [
      {
        ResourceType: 'volume',
        Tags: [
          {
            Key: 'Name',
            Value: VolumeAName
          }
        ]
      }
    ]
  };
}

module.exports = function instanceA (ec2, node_ssh, SSHKeyName, pathToSSHKey, SSHEnabledSecurityGroup, VolumeAName) {
    let resolveData = {};
    console.log('Begin creating instanceA...')
    return ec2.runInstances(instanceA_Params(SSHKeyName, SSHEnabledSecurityGroup)).promise()
      .then((data) => {
        let { InstanceId } = data.Instances[0];
        resolveData.InstanceId = InstanceId;
        console.log('Created instanceA', InstanceId);
  
        let { AvailabilityZone } = data.Instances[0].Placement,
          params = {
            AvailabilityZone,
            ...instanceA_VolumeParams(VolumeAName)
          };
        console.log('Creating new volume with params:', params);
        return ec2.createVolume(params).promise().then((data) => {
          console.log('New Volume Created!', data);
          let { VolumeId } = data,
            attachVolumeParams = {
              Device: '/dev/xvdb',
              VolumeId,
              InstanceId
            };
          resolveData.VolumeIds = [VolumeId];
          console.log("Waiting for InstanceA to finish initializing...");
          return ec2.waitFor('instanceStatusOk', { InstanceIds: [InstanceId] }).promise()
            .then(() => console.log('Instance is Ready! Attaching Volume with params:', attachVolumeParams))
            .then(() =>
              ec2.attachVolume(attachVolumeParams).promise()
                .then((data) => console.log('Volume attached!', data))
            ).then(() => console.log('Obtaining PublicDnsName to ssh into running instance...'))
            .then(() => ec2.describeInstances({ InstanceIds: [InstanceId] }).promise())
            .then((instanceData) => {
              console.log('returned data from describeInstances', instanceData);
              let { PublicDnsName } = instanceData.Reservations[0].Instances[0],
                ssh = new node_ssh(),
                connectParams = {
                  host: PublicDnsName,
                  username: 'ec2-user',
                  privateKey: pathToSSHKey
                };
              resolveData.sshParams = connectParams;
              console.log('connect to running instance via ssh', connectParams)
              return ssh.connect(connectParams)
                .then(() => console.log('install package needed to create xfs filesystem...'))
                .then(() => ssh.execCommand('sudo yum install -y xfsprogs'))
                .then(() => console.log('format /dev/xvdb with xfs filesystem...'))
                .then(() => ssh.execCommand('sudo mkfs -t xfs /dev/xvdb'))
                .then(() => console.log('make sure /dataup directory exists...'))
                .then(() => ssh.execCommand('sudo mkdir -p /dataup'))
                .then(() => console.log('check fstab for /dev/xvdb entry...'))
                .then(() => ssh.execCommand('cat /etc/fstab | grep "/dev/xvdb')
                  .then((res) => {
                    console.log('make sure no fstab entry for /dev/xvdb exists...');
                    if (/\/dev\/xvdb/.test(res.stdout)) {
                      console.error('found unexpected /dev/xvdb entry in fstab, will not update, but continuing', res);
                      return;
                    }
                    console.log('backup fstab before updating...');
                    return ssh.execCommand('sudo cp /etc/fstab /etc/fstab.bak')
                      .then(() => console.log('append /dev/xvdb mount to fstab'))
                      .then(() => ssh.execCommand('echo "/dev/xvdb\t/dataup\txfs\tdefaults,nofail" | sudo tee -a /etc/fstab > /dev/null'))
                      .then(() => console.log('confirm fstab file has no errors'))
                      .then(() => ssh.execCommand('sudo mount -a')
                        .then((res) => {
                          console.log("STDOUT: ", res.stdout);
                          res.stdout && console.log(res.stdout.length);
                          console.log("STDERR: ", res.stderr);
                          res.stderr && console.log(res.stderr.length);
                          if (res.stdout || res.stderr) {
                            throw new Error('fstab has been updated improperly! aborting', res);
                          }
                        })
                      );
                  })
                ).finally(() => ssh.dispose()); // cleanup ssh connection
            }).then(() => console.log('Rebooting instance...'))
            .then(() => {
              ec2.rebootInstances({ InstanceIds: [InstanceId] })
            });
        });
      }).then(() => resolveData) // resolve with the InstanceId and sshParams
      .catch((err) => {
        console.error(err, err.stack);
        throw err;
      });
  }