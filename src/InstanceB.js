function instanceB_Params(SSHKeyName, SSHEnabledSecurityGroup) {
    return {
        ImageId: 'ami-05a36d3b9aa4a17ac',
        InstanceType: 't2.micro',
        KeyName: SSHKeyName,
        MinCount: 1,
        MaxCount: 1,
        SecurityGroupIds: [SSHEnabledSecurityGroup],
        BlockDeviceMappings: [
            {
                // Root Volume
                DeviceName: "/dev/sda1",
                Ebs: {
                    DeleteOnTermination: true,
                    VolumeSize: 9 // GiB
                }
            }
        ],
        TagSpecifications: [
            {
                ResourceType: 'instance',
                Tags: [
                    {
                        Key: 'Name',
                        Value: 'InstanceB'
                    }
                ]
            }
        ]
    };
}
function instanceB_VolumeParams(volumeName) {
    return {
        Encrypted: true,
        Size: 1, //GiB
        VolumeType: 'gp2',
        TagSpecifications: [
            {
                ResourceType: 'volume',
                Tags: [
                    {
                        Key: 'Name',
                        Value: volumeName
                    }
                ]
            }
        ]
    };
}

module.exports = function instanceB(ec2, node_ssh, SSHKeyName, pathToSSHKey, SSHEnabledSecurityGroup, VolumeBNames) {
    let resolveData = {};
    console.log('Begin creating instanceB...')
    return ec2.runInstances(instanceB_Params(SSHKeyName, SSHEnabledSecurityGroup)).promise()
        .then((data) => {
            let { InstanceId } = data.Instances[0];
            resolveData.InstanceId = InstanceId;
            console.log('Created instanceB', InstanceId);

            let { AvailabilityZone } = data.Instances[0].Placement,
                vol1params = {
                    AvailabilityZone,
                    ...instanceB_VolumeParams(VolumeBNames[0])
                },
                vol2params = {
                    AvailabilityZone,
                    ...instanceB_VolumeParams(VolumeBNames[1])
                };
            console.log('Creating 2 new volumes with params:', vol1params, vol2params);
            return Promise.all([
                ec2.createVolume(vol1params).promise(),
                ec2.createVolume(vol2params).promise()
            ]).then(([vol1Data, vol2Data]) => {
                console.log('New Volumes Created!', vol1Data, vol2Data);
                let vol1ID = vol1Data.VolumeId,
                    vol2ID = vol2Data.VolumeId,
                    attachVolume1Params = {
                        Device: '/dev/sde',
                        VolumeId: vol1ID,
                        InstanceId
                    },
                    attachVolume2Params = {
                        Device: '/dev/sdf',
                        VolumeId: vol2ID,
                        InstanceId
                    };
                resolveData.VolumeIds = [vol1ID, vol2ID]
                console.log("Waiting for InstanceB to finish initializing...");
                return ec2.waitFor('instanceStatusOk', { InstanceIds: [InstanceId] }).promise()
                    .then(() => console.log('Instance is Ready! Attaching Volumes with params:', attachVolume1Params, attachVolume2Params))
                    .then(() =>
                        Promise.all([
                            ec2.attachVolume(attachVolume1Params).promise()
                                .then((data) => console.log('Volume1 attached!', data)),
                            ec2.attachVolume(attachVolume2Params).promise()
                                .then((data) => console.log('Volume2 attached!', data))
                        ])
                    ).then(() => console.log('Obtaining PublicDnsName to ssh into running instance...'))
                    .then(() => ec2.describeInstances({ InstanceIds: [InstanceId] }).promise())
                    .then((instanceData) => {
                        console.log('returned data from describeInstances', instanceData);
                        let { PublicDnsName } = instanceData.Reservations[0].Instances[0],
                            ssh = new node_ssh(),
                            connectParams = {
                                host: PublicDnsName,
                                username: 'ubuntu',
                                privateKey: pathToSSHKey
                            };
                        resolveData.sshParams = connectParams;
                        console.log('connect to running instance via ssh', connectParams);
                        return ssh.connect(connectParams)
                            .then(() => console.log('install package needed to create raid and xfs filesystem...'))
                            .then(() => ssh.execCommand('sudo apt-get update && sudo apt-get install -y --no-install-recommends mdadm xfsprogs'))
                            .then(() => console.log('create RAID 0 striped array /dev/md0 using /dev/xvde and /dev/xvdf...'))
                            .then(() => ssh.execCommand('sudo mdadm --create /dev/md0 --level=0 --raid-devices=2 /dev/xvde /dev/xvdf'))
                            .then(() => console.log('format /dev/md0 with xfs filesystem...'))
                            .then(() => ssh.execCommand('sudo mkfs -t xfs /dev/md0'))
                            .then(() => console.log('make sure /datadown directory exists...'))
                            .then(() => ssh.execCommand('sudo mkdir -p /datadown'))
                            .then(() => console.log('check fstab for /dev/md0 entry...'))
                            .then(() => ssh.execCommand('cat /etc/fstab | grep "/dev/md0')
                                .then((res) => {
                                    if (/\/dev\/md0/.test(res.stdout)) {
                                        console.error('found unexpected /dev/md0 entry in fstab, will not update, but continuing', res);
                                        return;
                                    }
                                    console.log('backup fstab before updating...');
                                    return ssh.execCommand('sudo cp /etc/fstab /etc/fstab.bak')
                                        .then(() => console.log('append /dev/md0 mount to fstab'))
                                        .then(() => ssh.execCommand('echo "/dev/md0\t/datadown\txfs\tdefaults,nofail" | sudo tee -a /etc/fstab > /dev/null'))
                                        .then(() => console.log('confirm fstab file has no errors'))
                                        .then(() => ssh.execCommand('sudo mount -a')
                                            .then((res) => {
                                                console.log("STDOUT: ", res.stdout);
                                                res.stdout && console.log(res.stdout.length);
                                                console.log("STDERR: ", res.stderr);
                                                res.stderr && console.log(res.stderr.length);
                                                if (res.stdout || res.stderr) {
                                                    throw new Error('fstab has been updated improperly! aborting', res);
                                                }
                                            })
                                        );
                                })
                            ).finally(() => ssh.dispose()); // cleanup ssh connection
                    }).then(() => console.log('Rebooting instance...'))
                    .then(() => ec2.rebootInstances({ InstanceIds: [InstanceId] }));
            });
        }).then(() => resolveData) // resolve with the InstanceId and sshParams
        .catch((err) => {
            console.error(err, err.stack);
            throw err;
        });
}