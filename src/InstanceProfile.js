function createRole(iam, uniqueID) {
    const trustPolicy = {
      "Version": "2012-10-17",
      "Statement": [
        {
          "Effect": "Allow",
          "Principal": { "Service": "ec2.amazonaws.com" },
          "Action": "sts:AssumeRole"
        }
      ]
    },
      roleAccessPolicy = {
        "Version": "2012-10-17",
        "Statement": [
          {
            "Effect": "Allow",
            "Action": ["s3:*"],
            "Resource": ["*"]
          }
        ]
      },
      createParams = {
        AssumeRolePolicyDocument: JSON.stringify(trustPolicy),
        RoleName: `S3Access-${uniqueID}`
      },
      accessParams = {
        PolicyDocument: JSON.stringify(roleAccessPolicy),
        PolicyName: `S3AccessPolicy-${uniqueID}`,
        RoleName: createParams.RoleName
      };
    console.log(`Creating role ${createParams.RoleName}...`);
    return iam.createRole(createParams).promise()
      .then((data) => {
        console.log('Role creation successful! ', data);
        console.log('Adding S3 access role policy...');
        return iam.putRolePolicy(accessParams).promise()
      }).then((data) => {
        console.log('Role policy updated! ', data);
        return createParams.RoleName;
      });
  }
  
  function createInstanceProfile(iam, RoleName) {
    const createParams = {
      InstanceProfileName: RoleName
    },
      addRoleToIPParams = {
        InstanceProfileName: RoleName,
        RoleName
      };
    console.log(`Creating instance profile ${RoleName}`);
    return iam.createInstanceProfile(createParams).promise()
      .then((data) => {
        console.log('Instance profile created! ', data);
        console.log(`Adding role '${RoleName}' to instance profile ${RoleName}...`);
        return iam.addRoleToInstanceProfile(addRoleToIPParams).promise();
      }).then((data) => {
        console.log('Role successfully added! ', data);
        return addRoleToIPParams.InstanceProfileName;
      });
  }
  
  function addIPToInstance(ec2, InstanceId, IPName) {
    const params = {
      IamInstanceProfile: {
        Name: IPName
      },
      InstanceId
    };
    console.log(`Associating instance profile ${IPName} with instance ${InstanceId}...`);
    return ec2.associateIamInstanceProfile(params).promise()
      .then((data) => console.log('Association successful! ', data))
      .catch(() => {
        console.log('Error when associating instance profile, attempt to replace instead...');
        return ec2.describeIamInstanceProfileAssociations({ Filters: [{ Name: 'instance-id', Values: [InstanceId] }] }).promise()
          .then(({ IamInstanceProfileAssociations }) => {
            if (IamInstanceProfileAssociations.length) {
              let { AssociationId } = IamInstanceProfileAssociations[0],
                replaceParams = {
                  AssociationId,
                  IamInstanceProfile: {
                    Name: IPName
                  }
                };
              console.log(`Found AssociationId ${AssociationId} for instance ${InstanceId}, replacing...`);
              return ec2.replaceIamInstanceProfileAssociation(replaceParams).promise();
            }
            throw new Error(`Couldn't find existing AssociationId for instance ${InstanceId}`);
          }).then((data) => {
            console.log('Replacement of instance profile successful! ', data);
          });
      });
  }

  module.exports = {
      createRole,
      createInstanceProfile,
      addIPToInstance
  }