function policyParams(Bucket) {
    return {
        Bucket,
        Policy: JSON.stringify({
            "Version": "2008-10-17",
            "Id": "https_Only",
            "Statement": [
                {
                    "Sid": "AddPerm",
                    "Effect": "Allow",
                    "Principal": {
                        "AWS": "*"
                    },
                    "Action": "s3:GetObject",
                    "Resource": `arn:aws:s3:::${Bucket}/*`,
                    "Condition": {
                        "Bool": {
                            "aws:SecureTransport": "true"
                        }
                    }
                }
            ]
        })
    };
}

function sseParams(Bucket) {
    return {
        Bucket,
        ServerSideEncryptionConfiguration: {
            Rules: [
                {
                    ApplyServerSideEncryptionByDefault: {
                        SSEAlgorithm: 'AES256'
                    }
                }
            ]
        }
    };
}

module.exports = function createS3Bucket(s3, Bucket) {
    console.log('Creating S3 Bucket with params: ', { Bucket });
    return s3.createBucket({ Bucket }).promise()
        .then(({ Location }) => console.log('Bucket creation successful! Location: ', Location))
        .then(() => console.log('Ensuring bucket exists before continuing...'))
        .then(() => s3.waitFor('bucketExists', { Bucket }).promise())
        .then(() => console.log('Enforce https traffic only to bucket...'))
        .then(() => s3.putBucketPolicy(policyParams(Bucket)).promise())
        .then(() => console.log('Adding server-side encryption to bucket...'))
        .then(() => s3.putBucketEncryption(sseParams(Bucket)).promise())
        .then(() => s3.getBucketPolicy({ Bucket }).promise())
        .then((data) => console.log('Bucket Policy: ', data))
        .catch((err) => {
            console.error(err, err.stack);
            throw err;
        });
}