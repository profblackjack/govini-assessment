module.exports = function s3DataTransferTest (node_ssh, Bucket, region, InstanceA_sshParams, InstanceB_sshParams) {
    let ssh = new node_ssh(),
      test_filename = "Testfile.txt",
      test_fileContent = "This is a test";

    console.log('Setup complete, performing aws-cli file transfer test using s3 bucket...');
    console.log('SSH into InstanceA...');
    return ssh.connect(InstanceA_sshParams)
      .then(() => console.log('Print contents of /dataup ...'))
      .then(() => ssh.execCommand('ls -al /dataup').then((res) => console.log(res.stdout)))
      .then(() => console.log(`Create test file ${test_filename} with content "${test_fileContent}"...`))
      .then(() => ssh.execCommand(`echo ${test_fileContent} | sudo tee /dataup/${test_filename}`))
      .then(() => console.log('Print contents of /dataup ...'))
      .then(() => ssh.execCommand('ls -al /dataup').then((res) => console.log(res.stdout)))
      .then(() => console.log(`Push ${test_filename} to s3 bucket ${Bucket} ...`))
      .then(() => ssh.execCommand(`aws s3 cp /dataup/${test_filename} s3://${Bucket}/`))
      .then(() => console.log('Print contents of bucket...'))
      .then(() => ssh.execCommand(`aws s3 ls s3://${Bucket}`).then((res) => console.log(res.stdout)))
      .then(() => console.log('Exit InstanceA and SSH into InstanceB'))
      .then(() => ssh.dispose())
      .then(() => ssh.connect(InstanceB_sshParams))
      .then(() => console.log('Print contents of /datadown ... '))
      .then(() => ssh.execCommand('ls -al /datadown').then((res) => console.log(res.stdout)))
      .then(() => console.log('Ensure aws cli is installed ...'))
      .then(() => ssh.execCommand('sudo apt-get update && sudo apt-get install -y awscli'))
      .then(() => console.log(`Copy ${test_filename} from s3 bucket ${Bucket} ...`))
      .then(() => ssh.execCommand(`sudo aws s3 cp --region ${region} s3://${Bucket}/${test_filename} /datadown/${test_filename}`))
      .then(() => console.log('Print contents of /datadown ...'))
      .then(() => ssh.execCommand('ls -al /datadown').then((res) => console.log(res.stdout)))
      .then(() => console.log(`Print contents of ${test_filename} ...`))
      .then(() => ssh.execCommand(`cat /datadown/${test_filename}`).then((res) => console.log(res.stdout)))
      .then(() => console.log('Close ssh connection ...'))
      .finally(() => ssh.dispose());
}