function emptyAndDeleteBucket(s3, Bucket) {
    return s3.listObjectsV2({ Bucket }).promise()
        .then(({ Contents }) => {
            console.log('Found items in bucket, deleting first ...');
            if (Contents && Contents.length) {
                console.log(Contents);
                let Objects = Contents.map(({ Key }) => {
                    return {Key};
                });
                return s3.deleteObjects({ Bucket, Delete: { Objects } }).promise()
                    .then(() => console.log('Items deleted, deleting bucket ... '));
            }
            console.log('No items found, deleting bucket ...');
        }).then(() => s3.deleteBucket({ Bucket }).promise());
}

function teardownInstancePolicy(iam, RoleName) {
    console.log(`Remove role ${RoleName} from instance profile ${RoleName} ...`);
    return iam.removeRoleFromInstanceProfile({ RoleName, InstanceProfileName: RoleName }).promise()
        .then(() => console.log('Delete instance profile ...'))
        .then(() => iam.deleteInstanceProfile({ InstanceProfileName: RoleName }).promise())
        .then(() => console.log('Delete all policies associated with role ...'))
        .then(() => iam.listRolePolicies({ RoleName }).promise())
        .then(({ PolicyNames }) => {
            if (PolicyNames && PolicyNames.length) {
                console.log('Policies found, deleting ...');
                let promises = PolicyNames.map((PolicyName) => iam.deleteRolePolicy({ RoleName, PolicyName }).promise())
                return Promise.all(promises).then(() => console.log('Policies deleted, deleting role ...'));
            }
            console.log('No Policies found, deleting role ...');
        }).then(() => iam.deleteRole({ RoleName }).promise());
}

module.exports = function teardown(ec2, s3, iam, InstanceIds, Bucket, RoleName, VolumeIds) {
    console.log(`Beginning teardown, terminate instances with IDs ${InstanceIds} ...`);
    return ec2.terminateInstances({ InstanceIds }).promise()
        .then(() => console.log(`Termination initialized. Empty and delete bucket ${Bucket} ...`))
        .then(() => emptyAndDeleteBucket(s3, Bucket))
        .then(() => console.log(`Bucket deleted. Teardown InstancePolicy and Role ${RoleName} ...`))
        .then(() => teardownInstancePolicy(iam, RoleName))
        .then(() => console.log('Instance Profile teardown finished. Wait for instance termination to finish ...'))
        .then(() => ec2.waitFor('instanceTerminated', { InstanceIds }).promise())
        .then(() => console.log(`Instances terminated. Delete associated volumes ${VolumeIds} ...`))
        .then(() => Promise.all(VolumeIds.map((VolumeId) => ec2.deleteVolume({ VolumeId }).promise())))
        .then(() => console.log('Volumes deleted. Teardown Complete!'))
}