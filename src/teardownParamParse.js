const jsonpack = require('jsonpack');

module.exports = {
    buildTeardownParams (InstanceIds, Bucket, RoleName, VolumeIds){
        console.log('Packing params necessary for teardown ...');
        return jsonpack.pack({InstanceIds, Bucket, RoleName, VolumeIds});
    },
    parseTeardownArg (package) {
        console.log('Unpacking params for teardown ... ');
        let unpacked = jsonpack.unpack(package);
        if(!unpacked.InstanceIds || !unpacked.Bucket || !unpacked.RoleName || !unpacked.VolumeIds) {
            throw new Error(`Supplied pack is invalid! Cannot perform teardown.\n${package}`);
        }
        return unpacked;
    }
}